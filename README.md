################# Conky Script with 4 Cores and without any luas-scripts ##########################

	invert displays:	
				- system infos			
				- 4Cores + Mhz + activity + temp + graph
				- GPU & HDD/SSD temp
				- Ram + scale
				- HDD/SSD sizes + 2 HDD if active
				- 7 processes on top + name + pid + mem
				- active firejails
				- IP local + online (switch between Wi-Fi & LAN)
				- up-/download speed (switch between Wi-Fi & LAN)
				- up-/download graph (switch between Wi-Fi & LAN)
				- portmon local (0-4) + global (0-11)
				- traffic down/up + day + week + month (switch between Wi-Fi & LAN)


	must be installed:	
				- conky-all
				- hddtemp
				- vnstat
				- sensors


	how to use:		- copy ".conkyrc" to your home folder and run "conky" in a terminal